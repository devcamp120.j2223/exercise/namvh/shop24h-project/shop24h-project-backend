const productModel = require("../models/productModel");
const productTypeModel = require("../models/productTypeModel");
const mongoose = require("mongoose");

const createProduct = (req, res) => {
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(bodyReq.type)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: "type is required",
    });
  }

  if (!bodyReq.name) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `Name is required`,
    });
  }

  if (!bodyReq.brand) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `Name is required`,
    });
  }

  if (!bodyReq.imageUrl) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `imageUrl is required`,
    });
  }
  if (!(Number.isInteger(bodyReq.buyPrice) && bodyReq.buyPrice > 0)) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `buyPrice is required`,
    });
  }
  if (!Number.isInteger(bodyReq.promotionPrice)) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `promotionPrice is required`,
    });
  }
  if (!Number.isInteger(bodyReq.amount)) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `amount is required`,
    });
  }

  const createBody = {
    _id: mongoose.Types.ObjectId(),
    name: bodyReq.name,
    brand: bodyReq.brand,
    description: bodyReq.description,
    type: bodyReq.type,
    imageUrl: bodyReq.imageUrl,
    buyPrice: bodyReq.buyPrice,
    promotionPrice: bodyReq.promotionPrice,
    amount: bodyReq.amount,
  };

  productTypeModel.findOne(
    { _id: bodyReq.type },
    (errFind, productTypeData) => {
      if (errFind) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: errFind.message,
        });
      } else {
        if (!productTypeData) {
          return res.status(404).json({
            status: "Error 404 ",
            message: `productTypeData is not found`,
          });
        } else {
          productModel.create(createBody, (err, newProductCreated) => {
            console.log(createBody);
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal Server Error",
                message: err.message,
              });
            } else {
              return res.status(201).json({
                status: "Create product successfully!!!",
                newProductCreated,
              });
            }
          });
        }
      }
    }
  );
};

const getAllProduct = (req, res) => {
  const { name, brand, min, max, limit, skip, type } = req.query;
  // let limit = req.query.limit;

  const condition = {};
  if (name) {
    const regex = new RegExp(`${name}`);
    condition.name = regex;
  }

  if (type) {
    condition.type = type;
  }

  if (brand) {
    const regex = new RegExp(`${brand}`);
    condition.brand = regex;
  }

  if (min) {
    condition.buyPrice = {
      ...condition.buyPrice,
      $gte: min,
    };
  }

  if (max) {
    condition.buyPrice = {
      ...condition.buyPrice,
      $lte: max,
    };
  }

  productModel
    .find(condition)
    .populate("type")
    .sort({ timeCreated: -1 })
    .skip(parseInt(skip) || 0)
    .limit(parseInt(limit) || 0)
    .exec((err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get All ProductType info successfully!!!",
          data: data,
        });
      }
    });
};

const getProductById = (req, res) => {
  let productId = req.params.productId;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  } else {
    productModel.findById(productId, (err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get  ProductType by Id  successfully",
          data: data,
        });
      }
    });
  }
};

const updateProduct = (req, res) => {
  let productId = req.params.productId;
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  }

  const updateBody = {
    name: bodyReq.name,
    brand: bodyReq.brand,
    description: bodyReq.description,
    type: bodyReq.type,
    imageUrl: bodyReq.imageUrl,
    buyPrice: bodyReq.buyPrice,
    promotionPrice: bodyReq.promotionPrice,
    amount: bodyReq.amount,
  };

  productModel.findByIdAndUpdate(productId, updateBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Update  ProductType by Id  successfully",
        data: data,
      });
    }
  });
};

const deleteProduct = (req, res) => {
  let productId = req.params.productId;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  }

  productModel.findByIdAndDelete(productId, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(402).json({
        status: "Delete  ProductType by Id  successfully",
        data: data,
      });
    }
  });
};

module.exports = {
  createProduct: createProduct,
  getAllProduct: getAllProduct,
  getProductById: getProductById,
  updateProduct: updateProduct,
  deleteProduct: deleteProduct,
};
