const orderModel = require("../models/orderModel");
const customerModel = require("../models/customerModel");
const mongoose = require("mongoose");

const createOrderOfCustomer = (req, res) => {
  let customerId = req.params.customerId;
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "customerId  is invalid",
    });
  }

  const createBody = {
    _id: mongoose.Types.ObjectId(),
    orderDate: bodyReq.orderDate,
    shippedDate: bodyReq.shippedDate,
    note: bodyReq.note,
    orderDetail: bodyReq.orderDetail,
    cost: bodyReq.cost,
  };

  orderModel.create(createBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      customerModel.findByIdAndUpdate(
        customerId,
        {
          $push: { orders: data._id },
        },
        (err, updateCustomer) => {
          if (err) {
            return res.status(500).json({
              status: "Error 500: Internal server error",
              message: err.message,
            });
          } else {
            return res.status(201).json({
              status: "Create order successfully",
              data: data,
            });
          }
        }
      );
    }
  });
};

const getOrderById = (req, res) => {
  let orderId = req.params.orderId;

  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `orderId is in valid`,
    });
  } else {
    orderModel.findById(orderId, (err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get  Order by Id  successfully",
          data: data,
        });
      }
    });
  }
};

const updateOrder = (req, res) => {
  let orderId = req.params.orderId;
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `orderId is in valid`,
    });
  }

  const updateBody = {
    orderDate: bodyReq.orderDate,
    shippedDate: bodyReq.shippedDate,
    note: bodyReq.note,
    orderDetail: bodyReq.orderDetail,
    cost: bodyReq.cost,
  };

  orderModel.findByIdAndUpdate(orderId, updateBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Update  Order by Id  successfully",
        data: data,
      });
    }
  });
};

const deleteOrder = (req, res) => {
  let customerId = req.params.customerId;
  let orderId = req.params.orderId;

  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `orderId is in valid`,
    });
  }
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `customerId is in valid`,
    });
  }

  orderModel.findByIdAndDelete(orderId, (err) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      customerModel.findByIdAndUpdate(
        customerId,
        { $pull: { orders: orderId } },
        (err, updatedData) => {
          if (err) {
            return res.status(500).json({
              status: "Error 500: Internal Server Error",
              message: err.message,
            });
          } else {
            return res.status(204).json({
              status: "Delete Order Info & Customer Info successfully",
            });
          }
        }
      );
    }
  });
};

const getAllOrderOfCustomer = (req, res) => {
  let customerId = req.params.customerId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      status: "Error 400: Bad Request",
      message: "customerId is invalid",
    });
  }
  //B3: Thao tác với cơ sở dữ liệu
  customerModel
    .findById(customerId)
    .populate("orders")
    .exec((err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get data success",
          data: data.orders,
        });
      }
    });
};

const getAllOrder = (req, res) => {
  customerModel.find((err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Get All orders info successfully",
        data: data,
      });
    }
  });
};

module.exports = {
  createOrderOfCustomer: createOrderOfCustomer,
  getAllOrder: getAllOrder,
  getAllOrderOfCustomer: getAllOrderOfCustomer,
  getOrderById: getOrderById,
  updateOrder: updateOrder,
  deleteOrder: deleteOrder,
};
