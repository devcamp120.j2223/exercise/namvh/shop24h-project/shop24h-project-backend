const customerModel = require("../models/customerModel");
const mongoose = require("mongoose");

const createCustomer = (req, res) => {
  let bodyReq = req.body;

  if (!bodyReq.fullName) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `fullName is required`,
    });
  }
  if (!bodyReq.phone) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `phone is required`,
    });
  }
  if (!bodyReq.email) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `email is required`,
    });
  }

  const createBody = {
    _id: mongoose.Types.ObjectId(),
    fullName: bodyReq.fullName,
    phone: bodyReq.phone,
    email: bodyReq.email,
    address: bodyReq.address,
    city: bodyReq.city,
    country: bodyReq.country,
  };

  customerModel.create(createBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(201).json({
        status: "Create Customer successfully",
        data: data,
      });
    }
  });
};

const getAllCustomer = (req, res) => {
  customerModel.find((err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Get All Customer info successfully",
        data: data,
      });
    }
  });
};

const getCustomerById = (req, res) => {
  let customerId = req.params.customerId;

  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `customerId is in valid`,
    });
  } else {
    customerModel.findById(customerId, (err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get  Customer by Id  successfully",
          data: data,
        });
      }
    });
  }
};

const updateCustomer = (req, res) => {
  let customerId = req.params.customerId;
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `customerId is in valid`,
    });
  }

  const updateBody = {
    fullName: bodyReq.fullName,
    phone: bodyReq.phone,
    email: bodyReq.email,
    address: bodyReq.address,
    city: bodyReq.city,
    country: bodyReq.country,
  };

  customerModel.findByIdAndUpdate(customerId, updateBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Update  Customer by Id  successfully",
        data: data,
      });
    }
  });
};

const deleteCustomer = (req, res) => {
  let customerId = req.params.customerId;

  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `customerId is in valid`,
    });
  }

  customerModel.findByIdAndDelete(customerId, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(402).json({
        status: "Delete  Customer by Id  successfully",
        data: data,
      });
    }
  });
};

module.exports = {
  createCustomer: createCustomer,
  getAllCustomer: getAllCustomer,
  getCustomerById: getCustomerById,
  updateCustomer: updateCustomer,
  deleteCustomer: deleteCustomer,
};
