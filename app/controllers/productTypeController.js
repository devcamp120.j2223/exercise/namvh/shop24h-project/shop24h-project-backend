const productTypeModel = require("../models/productTypeModel");
const mongoose = require("mongoose");

const CreateProductType = (req, res) => {
  let bodyReq = req.body;

  if (!bodyReq.name) {
    return res.status(400).json({
      status: `Error 400: Bad request`,
      message: `Name is require and unique ${err.message}`,
    });
  }

  const createBody = {
    _id: mongoose.Types.ObjectId(),
    name: bodyReq.name,
    description: bodyReq.description,
  };

  productTypeModel.create(createBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(201).json({
        status: "Create ProductType successfully",
        data: data,
      });
    }
  });
};

const GetAllProductType = (req, res) => {
  const { name } = req.query;

  const condition = {};
  if (name) {
    const regex = new RegExp(`${name}`);
    condition.name = regex;
  }

  productTypeModel.find(condition, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Get All ProductType info successfully",
        data: data,
      });
    }
  });
};

const GetProductTypeByID = (req, res) => {
  let productId = req.params.productId;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  } else {
    productTypeModel.findById(productId, (err, data) => {
      if (err) {
        return res.status(500).json({
          status: "Error 500: Internal Server Error",
          message: err.message,
        });
      } else {
        return res.status(200).json({
          status: "Get  ProductType by Id  successfully",
          data: data,
        });
      }
    });
  }
};

const UpdateProductType = (req, res) => {
  let productId = req.params.productId;
  let bodyReq = req.body;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  }

  const updateBody = {
    name: bodyReq.name,
    description: bodyReq.description,
  };

  productTypeModel.findByIdAndUpdate(productId, updateBody, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(200).json({
        status: "Update  ProductType by Id  successfully",
        data: data,
      });
    }
  });
};

const DeleteProductType = (req, res) => {
  let productId = req.params.productId;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Error 400: Bad request",
      message: `ProductId is in valid`,
    });
  }

  productTypeModel.findByIdAndDelete(productId, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: err.message,
      });
    } else {
      return res.status(402).json({
        status: "Delete  ProductType by Id  successfully",
        data: data,
      });
    }
  });
};

module.exports = {
  CreateProductType: CreateProductType,
  GetAllProductType: GetAllProductType,
  GetProductTypeByID: GetProductTypeByID,
  UpdateProductType: UpdateProductType,
  DeleteProductType: DeleteProductType,
};
