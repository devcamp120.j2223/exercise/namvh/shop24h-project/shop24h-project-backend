const express = require("express");
const router = express.Router();
const middleWare = require("../middleware/middleware");

const {
  createOrderOfCustomer,
  getAllOrderOfCustomer,
  getAllOrder,
  getOrderById,
  updateOrder,
  deleteOrder,
} = require("../controllers/orderController");

router.use(middleWare);

router.post("/customers/:customerId/orders", createOrderOfCustomer);
router.get("/customers/:customerId/orders", getAllOrderOfCustomer);
router.get("/orders", getAllOrder);
router.get("/orders/:orderId", getOrderById);
router.put("/orders/:orderId", updateOrder);
router.delete("/customers/:customerId/orders/:orderId", deleteOrder);

module.exports = router;
